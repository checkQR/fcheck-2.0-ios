import 'package:flutter/material.dart';
import 'package:testqrcode/LoadingScreen/splash_screen.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:testqrcode/Color/getColorsHex.dart';
class Intro extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      pages: [
        PageViewModel(
          title: 'Giới thiệu',
          body: 'Fman là công ty đi đầu trong lĩnh vực ứng dụng công nghệ thông tin vào nông nghiệp. Chúng tôi kế thừa các thành quả từ hoạt động khoa học công nghệ của doanh nghiệp khoa học công nghệ Farmtech Vietnam.',
          image: Align(
            child: Image.asset('assets/logo/logoFmanScan.png', height: 180.0,fit: BoxFit.cover,),
            alignment: Alignment.bottomCenter,
          ),
          decoration: PageDecoration(
            pageColor: Color(getColor('#80C241')),
            /////////////////////////////////////////////////
            bodyTextStyle: TextStyle(color: Colors.white70, 
            fontSize: 20.0,
            fontFamily: 'Nunito-SemiBold'
            ),
            /////////////////////////////////////////////////
            titleTextStyle: TextStyle(color: Colors.white70,
            fontSize: 30.0,
            fontFamily: 'Nunito-SemiBold',
            fontWeight: FontWeight.bold
            )
          )
        ),
        PageViewModel(
          title: 'Fman Scan',
          body: 'Truy vấn thông tin sản phẩm nhanh chóng, bằng cách quét mã QR trên từng sản phẩm, phản hồi cực nhanh, chính xác, tiện lợi',
          image: Align(
            child: Image.asset('assets/intro/icon-qr.png', height: 175.0,),
            alignment: Alignment.bottomCenter,
          ),
          decoration: PageDecoration(
            /////////////////////////////////////////////////
            bodyTextStyle: TextStyle(color: Colors.black, 
            fontSize: 20.0,
            fontFamily: 'Nunito-SemiBold'
            ),
            /////////////////////////////////////////////////
            titleTextStyle: TextStyle(color: Colors.black,
            fontSize: 30.0,
            fontFamily: 'Nunito-SemiBold',
            fontWeight: FontWeight.bold
            )
          )
        )
      ],
      onDone: () => Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (context) => new SplashScreen())),
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Text('Bỏ qua', style: TextStyle(color: Colors.white70),),
      next: const Icon(Icons.arrow_forward, color: Colors.white70,),
      done: const Text('Bắt đầu', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black)),
    );
  }
}
