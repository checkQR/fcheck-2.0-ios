import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:testqrcode/LoadingScreen/intro.dart';
import 'package:testqrcode/LoadingScreen/splash_screen.dart';
class Splash extends StatefulWidget {
@override
SplashState createState() => new SplashState();
}

class SplashState extends State<Splash> {
Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);

    if (_seen) {
    Navigator.of(context).pushReplacement(
        new MaterialPageRoute(builder: (context) => new SplashScreen()));
    } else {
    prefs.setBool('seen', true);
    Navigator.of(context).pushReplacement(
        new MaterialPageRoute(builder: (context) => new Intro()));
    }
}

@override
void initState() {
    super.initState();
    new Timer(new Duration(seconds: 0), () {
    checkFirstSeen();
    });
}

@override
Widget build(BuildContext context) {
    return Scaffold(
    body: Center(
        child: Text('Loading...'),
    ),
    );
}
}