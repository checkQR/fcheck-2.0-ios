//import 'info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:testqrcode/Color/getColorsHex.dart';
import 'scan.dart';
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future<bool> _onBack(){
    return showDialog(
      context: context,
      builder: (context){
        return AlertDialog(
          title: Text('Bạn có chắc chắn muốn thoát?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                SystemNavigator.pop();
              },
            ),
            FlatButton(
              child: Text('Hủy'),
              onPressed: ()=>Navigator.pop(context, false),
            )
          ],
        );
      }
    );
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBack,
      child: Scaffold(
        appBar: new AppBar(
          title: Text('QR Code Scan', style: TextStyle(fontFamily: 'Nunito-SemiBold'),),
          backgroundColor: Color(getColor('#80C241')),
          automaticallyImplyLeading: false,
          /*actions: <Widget>[
            IconButton(
              icon: Icon(Icons.info),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (contex) => Info()));
              },
            )
          ],*/
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset('assets/barcode.png',
              height: 150.0,
              ),
              SizedBox(height: 20.0,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 80, vertical: 10.0),
                child: RaisedButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Scan()));
                  },
                  color: Color(getColor('#80C241')),
                  child: Text('Bắt đầu quét', style: TextStyle(color: Color(getColor('#F5F6FA')), fontFamily: 'Nunito-SemiBold', fontSize: 15.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );  
  } 
}